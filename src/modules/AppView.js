import React, {Component} from 'react';
import {StyleProvider} from 'native-base';
import {AsyncStorage} from "react-native";
import axios from "../plugins/axios";
import Navigator from './navigation/Navigator';
import getTheme from '../native-base-theme/components';
import material from '../native-base-theme/variables/material';

class AppView extends Component {
    constructor(props) {
        super(props);

        this.state = {}
    }

    render() {
        return (
            <StyleProvider style={getTheme(material)}>
                <Navigator
                    onNavigationStateChange={() => {}}
                    uriPrefix="/app"
                />
            </StyleProvider>
        )
    }
}

export default AppView
