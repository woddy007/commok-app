import React, {Component} from 'react';
import AppNavigator from './RootNavigation';
import {connect} from "react-redux";
import {compose, lifecycle} from "recompose";
import color from "../../styles/color";

class NavigatorView extends Component {
    render() {
        return (
            <AppNavigator/>
        )
    }
}

export default compose(
    connect(
        state => ({
            user: state.user
        }),
        dispatch => ({
            setUser: (user) => dispatch(setUser(user))
        }),
    ),
)(NavigatorView);
