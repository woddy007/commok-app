import { connect } from 'react-redux';
import {compose, lifecycle} from 'recompose';
import {Platform, UIManager} from 'react-native';

import AppView from './AppView';

export default compose(
    connect(
        state => ({}),
        dispatch => ({}),
    ),
    lifecycle({
        componentDidMount() {
            if (Platform.OS === 'android') {
                UIManager.getViewManagerConfig('setLayoutAnimationEnabledExperimental') && UIManager.getViewManagerConfig('setLayoutAnimationEnabledExperimental(true)');
            }
        },
    }),
)(AppView);
