import axios from 'axios'
import varibles from '../varibles'
import {AsyncStorage} from "react-native";

export default async (type, url, data) => {
    let params = {}

    url = varibles.API_URL + url

    switch (type) {
        case 'get': {
            if (data) {
                params['data'] = data
            }

            return axios({
                    method: 'get',
                    url: url,
                    ...params
                }
            )
        }
        case 'post': {
            return axios({
                method: 'post',
                url: url,
                data: data,
                ...params
            })
        }
        case 'put': {
            return axios({
                method: 'put',
                url: url,
                data: data,
                ...params
            })
        }
        case 'delete': {
            return axios({
                method: 'delete',
                url: url,
                ...params
            })
        }
    }
}
