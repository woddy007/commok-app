import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    ScrollView,
    TouchableOpacity,
    TouchableWithoutFeedback,
    RefreshControl,
    Animated
} from 'react-native';
import {AsyncStorage} from 'react-native';
import axios from "../../../plugins/axios";
import color from "../../../styles/color";
import Article from "../../../components/Article";
import ArticleModal from "../../../components/ArticleModal";
import {LoaderApp} from "../../../components/LoaderApp";
import {
    Button,
    Icon,
    Text,
    CheckBox,
} from "native-base";
import Modal from 'react-native-modal';
import {timestampToDateText, timestampToDate} from '../../../helper/formatting'

class Articles extends Component {
    constructor(props) {
        super(props);

        this.state = {
            currentNewsEdition: [],
            sort: {
                label: 'По дате ↓',
                value: ''
            },

            loading: true,
            refreshing: false,
            openArticleModal: false,
            openSortModal: false,
            openSourceModal: false,

            articles: [],
            articlesDefault: [],
            sites: [],
            keySites: [],
            sortItems: [
                {
                    label: 'По дате ↓',
                    value: ''
                },
                {
                    label: 'По количеству комментариев ↓',
                    value: '/top'
                },
            ],
            shortComments: [],

            articleModal: {},

            date: '',
            currentUrlApi: '',


            topHeaderY: new Animated.Value(-200),
        }

        this.refsHeaderCollape = React.createRef()
        this.refModalArticle = React.createRef()
        this.refsSort = React.createRef()
        this.scrollView = React.createRef()
    }

    componentDidMount = () => {
        this.getReadArticles()
        this.loadArticles()

        this.props.navigation.openHeaderCollapse = this._openHeaderCollapse.bind()
    }

    loadArticles = (urlPagination) => {
        let url = '/api'

        if ( this.scrollView.current && urlPagination ){
            LoaderApp.isChangeLoaderApp(true)
            this.scrollView.current.scrollTo({x: 0, y: 0, animated: false})
        }

        if (urlPagination) {
            url = urlPagination
        }

        axios('get', url).then(response => {
            let data = response.data

            let sites = data.sites
            let date = data.dates.currentDay
            let dateNext = data.dates.nextDate
            let datePrev = data.dates.previousDate
            let articles = data.articleItems
            let articlesDefault = data.articleItems
            let shortComments = data.topShort
            let nextLink = data.nextLink.split('https://commok.ru')[1]
            let previousLink = data.previousLink.split('https://commok.ru')[1]

            if (dateNext == date) {
                dateNext = ''
            }
            if (datePrev == date) {
                datePrev = ''
            }

            this.setState({
                articles: articles,
                articlesDefault: articlesDefault,
                shortComments: shortComments,
                sites: sites,
                loading: false,
                refreshing: false,
                currentNewsEdition: [],
                date: date,
                currentUrlApi: url,
                nextLink,
                previousLink,
                dateNext,
                datePrev
            })

            this.checkNewArticles()

            LoaderApp.isChangeLoaderApp(false)
        }).catch(error => {
            console.log('error: ', error.response)
            LoaderApp.isChangeLoaderApp(false)
        })
    }
    getArticle = (article) => {
        LoaderApp.isChangeLoaderApp(true)

        let articleModal = {
            id: article.id,
            title: article.title,
            url: article.url,
            comments: [],
            comments_count: article.comments_count
        }
        // let url = `/comments/get/${article.id}?j=1`
        let url = `/api/${this.state.date}/${article.id}`

        axios('get', url).then(response => {
            articleModal.comments = response.data

            this.setState({
                articleModal: articleModal,
                openArticleModal: true
            })

            this.setLocalStorageArticle(articleModal)
            LoaderApp.isChangeLoaderApp(false)
        })
    }
    getArticleFromShort = (article) => {
        console.log('article: ', article)

        article.id = article.article_item_id
        article.title = article.article_item_title
        article.url = article.article_item_url

        this.getArticle(article)
    }
    articleSort = (itemValue) => {
        this.setState({sort: itemValue})
    }
    setSortValue = (item) => {
        let url = this.state.currentUrlApi

        if ( url.indexOf('/top') > -1 ){
            url = url.replace(new RegExp("/top",'g'),"")
        }

        url += item.value

        this.loadArticles(url)

        this.setState({
            sort: item,
            openSortModal: false
        })
    }
    checkNewArticles = async () => {
        let oldArticlesSites = (await AsyncStorage.getItem('new-articles-sites')) ? JSON.parse(await AsyncStorage.getItem('new-articles-sites')) : {}
        let newArticlesSites = {}
        let sites = this.state.sites

        this.state.articlesDefault.map(article => {
            if (!newArticlesSites[article.site_id]) {
                newArticlesSites[article.site_id] = 0
            }
            newArticlesSites[article.site_id]++
        })

        if (Object.keys(oldArticlesSites).length > 0) {
            for (let key in oldArticlesSites) {
                if (newArticlesSites[key] && newArticlesSites[key] > oldArticlesSites[key]) {
                    sites.map(site => {
                        if (site.id == key) {
                            site['append'] = newArticlesSites[key] - oldArticlesSites[key]
                        }
                    })
                }
            }

            this.setState({
                sites
            })
        }

        await AsyncStorage.setItem('new-articles-sites', JSON.stringify(newArticlesSites))
    }
    setLocalStorageArticle = async (article) => {
        let readArticles = await AsyncStorage.getItem('read-articles')
        if (!readArticles) {
            readArticles = {}
        } else {
            readArticles = JSON.parse(readArticles)
        }

        readArticles[article.id] = {
            countComments: article.comments_count
        }

        await AsyncStorage.setItem('read-articles', JSON.stringify(readArticles))

        this.getReadArticles()
    }
    filterArticles = () => {
        let articles = []
        let currentNewsEdition = this.state.currentNewsEdition

        if (currentNewsEdition.length > 0) {
            this.state.articlesDefault.map(item => {
                if (currentNewsEdition.indexOf(item.site_id) > -1) {
                    articles.push(item)
                }
            })
        } else {
            articles = this.state.articlesDefault
            currentNewsEdition = []
        }

        this.setState({
            articles,
            currentNewsEdition
        })
    }
    _onRefresh = () => {
        this.setState({refreshing: true});

        this.loadArticles()
    }

    getReadArticles = async () => {
        let readArticles = await AsyncStorage.getItem('read-articles')

        if (!readArticles) {
            readArticles = {}
        } else {
            readArticles = JSON.parse(readArticles)
        }

        this.setState({
            readArticles
        })
    }

    setSite = (site) => {
        let currentNewsEdition = this.state.currentNewsEdition

        if (currentNewsEdition.indexOf(site.id) == -1) {
            currentNewsEdition.push(site.id)
        } else {
            currentNewsEdition.splice(currentNewsEdition.indexOf(site.id), 1)
        }

        this.setState({
            currentNewsEdition,
            openSourceModal: false
        })

        this.filterArticles()
    }

    _openHeaderCollapse = () => {
        let toValue = -200

        if (this.state.topHeaderY._value == -200) {
            toValue = 0;
        }

        Animated.timing(this.state.topHeaderY, {
            toValue: toValue,
            duration: 400
        }).start();
    }
    _openFilter = () => {
        this.setState({
            openSortModal: true
        })
    }
    _openSource = () => {
        this.setState({
            openSourceModal: true
        })
    }

    render() {
        let readArticles = this.state.readArticles

        if (this.state.loading) {
            return (
                <View style={[styles.page, {padding: 15}]}>
                    <Text style={{fontSize: 18, fontWeight: 'bold'}}>Идет загрузка...</Text>
                </View>
            )
        }
        return (
            <View style={styles.page}>
                <Animated.View
                    ref={this.refsHeaderCollape}
                    style={[
                        styles.headerCollapse,
                        {
                            transform: [
                                {
                                    translateY: this.state.topHeaderY,
                                },
                            ],
                        },
                    ]}
                >
                    <Text style={styles.headerCollapseText}>Актуальные комментарии к событиям дня</Text>
                    <Text style={styles.headerCollapseDate}>{timestampToDateText(this.state.date)} г.</Text>

                    <Button
                        full
                        bordered
                        warning
                        small
                        onPress={() => this.props.navigation.navigate('Search')}
                    >
                        <Text style={{color: color.secondary}}>Поиск</Text>
                    </Button>
                </Animated.View>

                <ScrollView
                    ref={this.scrollView}
                    style={{flex: 1}}
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{padding: 15}}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this._onRefresh}
                            colors={[color.primary]}
                            tintColor={color.primary}
                        />
                    }
                >
                    <Text style={styles.title}>
                        Комментарии к событиям за {timestampToDateText(this.state.date)} г.
                    </Text>
                    <View style={styles.viewShortComments}>
                        <Text style={styles.viewShortCommentsTitle}>Популярные короткие комментарии:</Text>
                        {
                            this.state.shortComments.map((comment, idx) => (
                                <TouchableOpacity
                                    key={'short-article-' + idx}
                                    onPress={() => this.getArticleFromShort(comment)}
                                >
                                    <Text
                                        style={styles.textShortComment}
                                        numberOfLines={1}
                                        ellipsizeMode="tail"
                                    >
                                        <Text>- { comment.content }</Text>
                                    </Text>
                                </TouchableOpacity>
                            ))
                        }
                    </View>

                    <View style={styles.viewTopSort}>
                        <View style={styles.sortView}>
                            <Text>Сортировка: </Text>

                            <TouchableOpacity
                                onPress={() => this._openFilter()}
                                style={{
                                    borderBottomWidth: 1,
                                    borderStyle: 'solid',
                                    borderColor: 'rgba(0, 0, 0, 0.2)'
                                }}
                            >
                                <Text>{this.state.sort.label}</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.sortView}>
                            <TouchableOpacity onPress={() => this._openSource()}>
                                <Text>Источники
                                    ({(this.state.currentNewsEdition.length > 0) ? this.state.currentNewsEdition.length : this.state.sites.length})</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    {
                        this.state.articles.map((article, idx) => (
                            <TouchableWithoutFeedback
                                key={'article-' + idx}
                                onPress={() => this.getArticle(article)}
                            >
                                <View style={[(readArticles[article.id]) ? {opacity: 0.6} : '']}>
                                    <Article
                                        article={article}
                                        readArticles={readArticles}
                                    />
                                </View>
                            </TouchableWithoutFeedback>
                        ))
                    }


                    <View style={styles.viewPagination}>
                        {
                            (this.state.dateNext) ? (
                                <TouchableWithoutFeedback onPress={() => this.loadArticles(this.state.nextLink)}>
                                    <View style={styles.viewPaginationButton}>
                                        <Text style={styles.viewPaginationText}>←{timestampToDate(this.state.dateNext)}</Text>
                                    </View>
                                </TouchableWithoutFeedback>
                            ) : (
                                <View style={styles.viewPaginationButton}></View>
                            )
                        }

                        <View style={[styles.viewPaginationButton, styles.viewPaginationButtonActive]}>
                            <Text
                                style={[styles.viewPaginationText, styles.viewPaginationTextActive]}>{this.state.date}</Text>
                        </View>

                        {
                            (this.state.datePrev) ? (
                                <TouchableWithoutFeedback onPress={() => this.loadArticles(this.state.previousLink)}>
                                    <View style={styles.viewPaginationButton}>
                                        <Text style={styles.viewPaginationText}>{timestampToDate(this.state.datePrev)}→</Text>
                                    </View>
                                </TouchableWithoutFeedback>
                            ) : (
                                <View style={styles.viewPaginationButton}></View>
                            )
                        }
                    </View>

                </ScrollView>


                <Modal
                    ref={this.refModalArticle}
                    isVisible={this.state.openArticleModal}
                    onBackButtonPress={() => {
                        this.setState({openArticleModal: false})
                    }}
                    onBackdropPress={() => {
                        this.setState({openArticleModal: false})
                    }}
                    onModalHide={() => {
                        this.setState({openArticleModal: false})
                    }}
                    backdropColor={color.primaryOverlay}
                    backdropOpacity={1}
                    scrollOffset={30}
                    backdropTransitionInTiming={0.01}
                    backdropTransitionOutTiming={0.01}
                    animationInTiming={0.01}
                    animationOutTiming={0.01}
                    useNativeDriver={true}
                >
                    <View style={styles.modalContent}>
                        <ArticleModal
                            article={this.state.articleModal}
                            closeModal={() => {
                                this.setState({openArticleModal: false})
                            }}
                        />
                    </View>
                </Modal>


                <Modal
                    isVisible={this.state.openSortModal}
                    onBackButtonPress={() => {
                        this.setState({openSortModal: false})
                    }}
                    onBackdropPress={() => {
                        this.setState({openSortModal: false})
                    }}
                    onModalHide={() => {
                        this.setState({openSortModal: false})
                    }}
                    backdropColor={color.primaryOverlay}
                    backdropOpacity={1}
                    backdropTransitionInTiming={0.01}
                    backdropTransitionOutTiming={0.01}
                    animationInTiming={0.01}
                    animationOutTiming={0.01}
                    useNativeDriver={true}
                >
                    <View style={styles.modalSort}>
                        <Text style={{fontSize: 18, fontWeight: 'bold', marginBottom: 10}}>Сортировка</Text>

                        {
                            this.state.sortItems.map((item, idx) => (
                                <TouchableOpacity
                                    style={styles.buttonSort}
                                    onPress={() => this.setSortValue(item)}
                                >
                                    <Text style={styles.buttonSortText}>{item.label}</Text>
                                </TouchableOpacity>
                            ))
                        }

                    </View>
                </Modal>

                <Modal
                    isVisible={this.state.openSourceModal}
                    onBackButtonPress={() => {
                        this.setState({openSourceModal: false})
                    }}
                    onBackdropPress={() => {
                        this.setState({openSourceModal: false})
                    }}
                    onModalHide={() => {
                        this.setState({openSourceModal: false})
                    }}
                    backdropColor={color.primaryOverlay}
                    backdropOpacity={1}
                    backdropTransitionInTiming={0.01}
                    backdropTransitionOutTiming={0.01}
                    animationInTiming={0.01}
                    animationOutTiming={0.01}
                    useNativeDriver={true}
                >
                    <View style={styles.modalSort}>
                        <Text style={{fontSize: 18, fontWeight: 'bold', marginBottom: 10}}>Источники</Text>

                        {
                            this.state.sites.map((site, idx) => (
                                <TouchableWithoutFeedback
                                    key={'site-' + idx}
                                    onPress={() => this.setSite(site)}
                                >
                                    <View
                                        style={{
                                            flexDirection: 'row',
                                            alignItems: 'center',
                                            paddingVertical: 5,
                                        }}
                                    >
                                        <Text style={{marginRight: 'auto'}}>{site.title}</Text>
                                        <CheckBox
                                            checked={this.state.currentNewsEdition.indexOf(site.id) > -1}
                                            onPress={() => this.setSite(site)}
                                        />
                                    </View>
                                </TouchableWithoutFeedback>
                            ))
                        }
                    </View>
                </Modal>

            </View>
        );
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: 'CommOk',
            headerRight: () => (
                <View
                    style={{marginRight: 15, flexDirection: 'row'}}
                >
                    {
                        (navigation.openHeaderCollapse) && (
                            <TouchableOpacity
                                style={styles.buttonHeaderMenu}
                                onPress={() => navigation.openHeaderCollapse()}
                            >
                                <Icon
                                    style={{
                                        fontSize: 22,
                                        color: 'rgba(255,255,255,.5)'
                                    }}
                                    name={'dehaze'}
                                    type={'MaterialIcons'}
                                />
                            </TouchableOpacity>
                        )
                    }
                </View>
            )
        };
    };
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
        overflow: 'hidden'
    },

    title: {
        fontSize: 20,
        marginBottom: 10,
        fontWeight: 'bold'
    },

    viewShortComments: {
        marginBottom: 20,
    },
    viewShortCommentsTitle: {
        color: '#6c757d',
        fontSize: 16,
        marginBottom: 2
    },
    textShortComment: {
        fontSize: 14,
        paddingVertical: 1
    },

    viewTopSort: {},

    newsEdition: {
        marginLeft: 10,
        flexDirection: 'row',
        alignItems: 'center'
    },
    newsEditionActive: {},
    newsEditionText: {
        fontSize: 18,
        fontWeight: '500',
        borderBottomWidth: 1,
        borderBottomColor: color.primary,
        borderStyle: 'dotted',
        color: color.primary,
    },
    newsEditionTextActive: {
        borderBottomColor: color.secondary,
        color: color.secondary
    },
    newsEditionCount: {},

    buttonHeaderMenu: {
        width: 40,
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',

        borderStyle: 'solid',
        borderColor: 'rgba(255,255,255,.1)',
        borderWidth: 1
    },

    headerCollapse: {
        position: 'absolute',
        left: 0,
        width: '100%',
        backgroundColor: color.primary,
        padding: 15,
        paddingTop: 0,
        zIndex: 999,

        transform: [
            {
                translateY: -200,
            },
        ],
    },
    headerCollapseText: {
        fontSize: 18,
        color: 'rgba(255,255,255,.54)'
    },
    headerCollapseDate: {
        fontSize: 17,
        color: 'white',

        marginBottom: 10
    },

    modalContent: {
        height: '95%',
        backgroundColor: 'white',
        overflow: 'hidden',
        borderRadius: 4
    },
    modalSort: {
        backgroundColor: 'white',
        overflow: 'hidden',
        borderRadius: 4,
        padding: 15
    },

    sortView: {
        alignItems: 'flex-start',
        marginBottom: 10,
    },

    buttonSort: {
        marginBottom: 5
    },
    buttonSortText: {
        fontSize: 16
    },

    viewPagination: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 20
    },
    viewPaginationButton: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewPaginationButtonActive: {},
    viewPaginationText: {
        color: color.primary,
        fontSize: 13,
        textAlign: 'center'
    },
    viewPaginationTextActive: {
        color: 'black'
    },
})

export default Articles
