import axios from "axios";

let timestampToDate = function (date) {
    if (!date){
        return null
    }

    var year = date.split('-')[0]
    var month = date.split('-')[1]
    var day = date.split('-')[2]


    var dateTime = day + '.' + month + '.' + year;

    return dateTime;
}
let timestampToTime = function (timestamp) {
    if (!timestamp){
        return null
    }

    var hour = time.split(':')[0]
    var min = time.split(':')[1]

    var dateTime = hour + ':' + min;

    return dateTime;
}
let timestampToDateTime = function (timestamp) {
    if (!timestamp){
        return null
    }

    var date = timestamp.split(' ')[0]
    var time = timestamp.split(' ')[1]

    var year = date.split('-')[0]
    var month = date.split('-')[1]
    var day = date.split('-')[2]

    var hour = time.split(':')[0]
    var min = time.split(':')[1]


    var dateTime = day + '.' + month + ' ' + hour + ':' + min;

    return dateTime;
}
let timestampToDateText = function (date) {
    if (!date){
        return null
    }

    var year = date.split('-')[0]
    var month = getMountTitle(date.split('-')[1])
    var day = date.split('-')[2]


    var dateTime = day + ' ' + month + ' ' + year;

    return dateTime;
}

let getMountTitle = function (month) {
    let numberMonth = Number(month)

    switch (numberMonth) {
        case 1: return 'января';
        case 2: return 'февраля';
        case 3: return 'марта';
        case 4: return 'апреля';
        case 5: return 'мая';
        case 6: return 'июня';
        case 7: return 'июля';
        case 8: return 'августа';
        case 9: return 'сентября';
        case 10: return 'октября';
        case 11: return 'ноября';
        case 12: return 'декабря';

        default: {
            return month
        }
    }
}

export { timestampToDate, timestampToTime, timestampToDateTime, timestampToDateText}
