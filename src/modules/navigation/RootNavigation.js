import React from 'react';
import {Image, TouchableOpacity, Text, Platform} from 'react-native';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import MainTabNavigator from './MainTabNavigator';

const stackNavigator = createStackNavigator(
    {
        Main: {
            screen: MainTabNavigator,
        },
    },
    {headerMode: 'none'},
);

export default createAppContainer(stackNavigator);
