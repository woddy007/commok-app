import { combineReducers } from 'redux';

import app from '../modules/AppState'

const rootReducer = combineReducers({
    app
});

export default rootReducer
