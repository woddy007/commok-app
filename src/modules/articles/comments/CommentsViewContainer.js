// @flow
import { compose } from 'recompose';
import { connect } from 'react-redux';

import CommentsView from './CommentsView';

export default compose(
  connect(
    state => ({}),
    dispatch => ({}),
  ),
)(CommentsView);
