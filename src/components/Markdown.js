// https://github.com/TitanInvest/react-native-easy-markdown

import React, {Component} from 'react';
import Markdown from 'react-native-easy-markdown';
import markdownStyle from '../styles/markdown'


class MarkdownContainer extends Component{
    constructor(props) {
        super(props);
    }

    render() {
        let styles = {...markdownStyle}

        return (
            <Markdown
                markdownStyles={styles}
                useDefaultStyles={false}
            >
                { this.props.children }
            </Markdown>
        )
    }
}


export default MarkdownContainer
