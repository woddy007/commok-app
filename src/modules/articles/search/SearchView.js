import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    ScrollView,
    TouchableOpacity,
    Animated,
} from 'react-native';
import {
    Item,
    Input,
    Text,
    Button,
    Form,
    Icon,
    Left,
    Title,
    Right,
    Body
} from 'native-base'
import {StatusBar} from 'react-native';
import color from "../../../styles/color";
import axios from "../../../plugins/axios";

class Search extends Component {
    constructor(props) {
        super(props);

        this.state = {
            searchQuery: '',

            result: [],

            loading: false,

            topHeaderY: new Animated.Value(0)
        }

        this.offset = 0
    }

    componentDidMount = () => {
    }

    getSearch = () => {
        let searchQuery = this.state.searchQuery

        this.setState({
            loading: true
        })

        axios('get', '/api/search?query=' + searchQuery).then(response => {
            console.log(response.data)

            this.setState({
                result: response.data,
                loading: false
            })
        }).catch(error => {
            console.log('error: ', error.response)
        })
    }

    onScroll = (event) => {
        let scrollContentOffsetTop = event.nativeEvent.contentOffset.y
        let toValue = this.state.topHeaderY._value

        if (scrollContentOffsetTop < this.offset || scrollContentOffsetTop < 50) {
            toValue = 0
        } else {
            toValue = -60
        }

        this.offset = scrollContentOffsetTop

        Animated.timing(this.state.topHeaderY, {
            toValue: toValue,
            duration: 80
        }).start();
    }

    render() {
        return (
            <View style={styles.page}>

                <Animated.View style={[styles.header, {
                    transform: [
                        {
                            translateY: this.state.topHeaderY,
                        },
                    ],
                }]}>
                    <Form style={{flex: 1}}>
                        <Item rounded>
                            <Input
                                value={this.state.searchQuery}
                                onChangeText={(searchQuery) => {
                                    this.setState({
                                        searchQuery
                                    })
                                }}
                                style={{
                                    height: 40,
                                    color: 'white'
                                }}
                                placeholder='Введите поисковый запрос'
                                placeholderTextColor={'rgba(255, 255, 255, 0.8)'}

                                returnKeyLabel='Поиск'
                                returnKeyType='search'
                                onSubmitEditing={() => this.getSearch()}
                            />
                        </Item>
                    </Form>
                    <Button
                        rounded
                        bordered
                        onPress={() => this.getSearch()}
                        style={{
                            height: 40,
                            width: 40,
                            marginLeft: 5,
                            justifyContent: 'center',
                            alignItems: 'center',
                            padding: 0
                        }}
                    >
                        <Icon
                            name={'ios-search'}
                            style={{
                                marginLeft: 0,
                                marginRight: 0,
                                color: 'white'
                            }}
                        />
                    </Button>
                </Animated.View>

                <ScrollView
                    style={{flex: 1}}
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={[
                        {padding: 15},
                        (this.state.topHeaderY._value == 0) ? {paddingTop: 60} : ''
                    ]}
                    onScroll={(event) => this.onScroll(event)}
                >
                    <View style={styles.content}>
                        {
                            (this.state.loading) && (
                                <>
                                    <Text style={styles.title}>Идет загрузка</Text>
                                </>
                            )
                        }
                        {
                            (!this.state.loading && this.state.result.length > 0 && this.state.result.length > 0) && (
                                <>
                                    <Text style={styles.title}>Результаты: ({this.state.result.length})</Text>

                                    {
                                        this.state.result.map((item, idx) => (
                                            <View
                                                key={'comment-' + idx}
                                                style={styles.comment}
                                            >
                                                <Text style={styles.commentTitle}>{ item.title }</Text>
                                                <Text
                                                    style={styles.commentComment}
                                                    numberOfLines={5}
                                                    ellipsizeMode="tail"
                                                >{ item.content }</Text>
                                            </View>
                                        ))
                                    }

                                </>
                            )
                        }
                        {
                            (!this.state.loading && this.state.result.length <= 0) && (
                                <>
                                    <Text style={styles.title}>Результаты: (0)</Text>
                                </>
                            )
                        }
                    </View>
                </ScrollView>
            </View>
        );
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: 'Поиск'
        };
    };
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
        overflow: 'hidden'
    },

    content: {},

    title: {
        fontSize: 20,
        marginBottom: 20
    },

    header: {
        position: 'absolute',
        zIndex: 999,

        flexDirection: 'row',
        paddingHorizontal: 15,
        paddingBottom: 10,
        backgroundColor: color.primary,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
    },


    comment: {
        backgroundColor: 'white',
        borderBottomWidth: 1,
        borderStyle: 'solid',
        borderColor: 'rgba(0,0,0,.125)',

        padding: 10
    },
    commentTitle: {
        fontSize: 18,
        marginBottom: 5,
        fontWeight: 'bold'
    },
    commentComment: {
        fontSize: 16
    }
})

export default Search
