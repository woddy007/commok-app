export default {
    primary: '#4D83D2',
    secondary: '#FCBA2E',
    positive: '#21BA45',
    page: '#f5f5f5',

    primaryOverlay: 'rgba(77, 131, 210, 0.6)'
}
