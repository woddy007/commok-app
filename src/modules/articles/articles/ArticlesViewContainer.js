// @flow
import { compose } from 'recompose';
import { connect } from 'react-redux';

import ArticlesView from './ArticlesView';

export default compose(
  connect(
    state => ({}),
    dispatch => ({}),
  ),
)(ArticlesView);
