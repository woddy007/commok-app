export default {
    h1: {},
    h2: {},
    h3: {},
    h4: {},
    h5: {},

    p: {},
    ul: {},
    li: {},

    heading: {
        fontSize: 25,
        fontWeight: '300'
    },

    text: {},
}
