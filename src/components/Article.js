import React, {Component} from 'react'
import {
    View,
    StyleSheet,
    Image,
} from 'react-native';
import {
    Icon,
    Text
} from 'native-base'
import color from "../styles/color";
import { LinearGradient } from 'expo-linear-gradient';
import {timestampToDateTime} from '../helper/formatting';

class Article extends Component{
    getCountMessageArticle = (article) => {
        let readArticles = this.props.readArticles
        if (readArticles){
            if (readArticles[article.id]) {
                let currentCommentCount = article.comments_count
                let oldCommentCount = readArticles[article.id].countComments
                let countNew = Number(currentCommentCount) - Number(oldCommentCount)

                if (countNew > 0) {
                    return (
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <Text style={{ color: color.secondary }}>
                                {article.comments_count}
                            </Text>
                            <Text style={{ color: color.positive, marginLeft: 3 }}>+{ countNew }</Text>
                        </View>
                    )
                }
            }
        }

        return (
            <View>
                <Text style={{ color: color.secondary }}>
                    {article.comments_count}
                </Text>
            </View>
        )
    }

    render() {
        let article = this.props.article

        // console.log('article: ', article)

        return (
            <View style={styles.article}>
                <View style={styles.articleTop}>
                    <View style={styles.articleSouseIcon}>
                        <Image
                            source={{uri: article.icon}}
                            style={styles.articleSouseIconImage}
                        />
                    </View>
                    <Icon
                        style={{
                            height: 20,
                            marginHorizontal: 5,
                            fontSize: 20,
                            lineHeight: 20
                        }}
                        name={'chat'}
                        type={'MaterialIcons'}
                    />
                    { this.getCountMessageArticle(article) }
                    <Text style={{marginLeft: 'auto'}}>
                        {timestampToDateTime(article.created_at)}
                    </Text>
                </View>
                <View style={styles.articleBody}>
                    <Text style={styles.articleTitle}>{article.title}</Text>
                    {
                        (article.first_comment) && (
                            <Text style={styles.articleComment}>{article.first_comment.content}</Text>
                        )
                    }
                </View>

                <LinearGradient
                    colors={['rgba(255,255,255,0)', 'rgba(255,255,255,1)']}
                    style={styles.articlePseudo}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    article: {
        backgroundColor: 'white',
        marginTop: 10,
        maxHeight: 300,
        overflow: 'hidden'
    },
    articleTop: {
        padding: 10,
        flexDirection: 'row',
        alignItems: 'center',

        borderBottomWidth: 1,
        borderStyle: 'solid',
        borderBottomColor: 'rgba(0, 0, 0, 0.12)'
    },
    articleBody: {
        padding: 10,
    },
    articleFooter: {
        padding: 10,
    },

    articleSouseIcon: {
        width: 20,
        height: 20
    },
    articleSouseIconImage: {
        flex: 1
    },

    articleTitle: {
        fontSize: 18,
        lineHeight: 22,
        color: '#37404D',
        fontWeight: 'bold',
        marginBottom: 10,
    },
    articleComment: {
        fontSize: 14,
        lineHeight: 20,
        color: '#77808C'
    },

    articlePseudo: {
        position: 'absolute',
        left: 0,
        top: 250,
        width: '100%',
        height: 55,
    }
})

export default Article
