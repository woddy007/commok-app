import React, {Component} from 'react'
import {
    View,
    StyleSheet,
    ScrollView
} from 'react-native';
import {
    Text,
    Icon,
    Button
} from 'native-base'
import color from "../styles/color";
import * as WebBrowser from 'expo-web-browser';
import MarkdownContainer from "./Markdown";
import {timestampToDateTime} from "../helper/formatting";

class ArticleModal extends Component {
    constructor(props) {
        super(props)

        this.state = {}
    }

    openSourceArticle = async (url) => {
        await WebBrowser.openBrowserAsync(url);
    }

    render() {
        let article = this.props.article

        console.log('article: ', article)

        return (
            <View style={styles.article}>
                <View style={styles.articleHeader}>
                    <Text style={styles.articleTitle}>{article.title}</Text>
                </View>
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    style={styles.articleContent}
                >
                    {
                        article.comments.map((comment, idx) => (
                            <View
                                key={'comment-' + idx}
                                style={styles.comment}
                            >
                                <View style={styles.commentHeader}>
                                    <Icon
                                        style={{
                                            height: 20,
                                            fontSize: 20,
                                            lineHeight: 20
                                        }}
                                        name={'person'}
                                        type={'MaterialIcons'}
                                    />
                                    <Text style={styles.commentUser}>{comment.author}</Text>
                                    {
                                        (comment.rating && comment.rating != '0' || comment.rating > 0) && (
                                            <Text style={styles.commentUserRate}>+{comment.rating}</Text>
                                        )
                                    }
                                    <Text style={styles.commentDate}>{timestampToDateTime(comment.created_at)}</Text>
                                </View>
                                <View style={styles.commentBody}>
                                    <MarkdownContainer>
                                        {comment.content}
                                    </MarkdownContainer>
                                </View>
                            </View>
                        ))
                    }
                </ScrollView>
                <View style={styles.articleFooter}>
                    <Button
                        style={{width: '50%'}}
                        full
                        bordered
                        onPress={() => this.openSourceArticle(article.url)}
                    >
                        <Text style={{color: color.primary}}>К источнику</Text>
                    </Button>
                    <View
                        style={{
                            height: '100%',
                            width: 1,
                            backgroundColor: 'rgba(0, 0, 0, 0.12)'
                        }}
                    />
                    <Button
                        style={{width: '50%'}}
                        full
                        onPress={() => this.props.closeModal()}
                    >
                        <Text style={{color: 'white'}}>Закрыть</Text>
                    </Button>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    article: {
        flex: 1
    },

    articleHeader: {
        padding: 15,

        borderBottomWidth: 1,
        borderStyle: 'solid',
        borderBottomColor: 'rgba(0, 0, 0, 0.12)'
    },
    articleContent: {
        flex: 1
    },
    articleFooter: {
        flexDirection: 'row'
    },

    articleTitle: {
        fontSize: 18,
        lineHeight: 20,
        fontWeight: 'bold',
        color: '#37404D',
    },


    comment: {
        padding: 15,

        borderBottomWidth: 1,
        borderStyle: 'solid',
        borderBottomColor: 'rgba(0, 0, 0, 0.12)'
    },
    commentUser: {
        marginLeft: 5,
    },
    commentUserRate: {
        marginLeft: 5,
        color: color.positive
    },
    commentDate: {
        marginLeft: 5
    },
    commentHeader: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'center',
        marginBottom: 5
    },
    commentBody: {},
    commentComment: {
        fontSize: 15,
        lineHeight: 17
    },
})

export default ArticleModal
