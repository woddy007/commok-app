import {Provider} from 'react-redux';
import React, {Component} from 'react';
import {View, ActivityIndicator, StatusBar, StyleSheet} from 'react-native';
import {PersistGate} from 'redux-persist/src/integration/react';
import {store, persistor} from './src/redux/store';
import AppView from './src/modules/AppViewContainer';
import * as Font from 'expo-font';
import {Ionicons} from '@expo/vector-icons';
import DropdownAlert from "react-native-dropdownalert";
import {DropDownHolder} from './src/components/DropDownAlert'
import {LoaderApp} from "./src/components/LoaderApp";
import Spinner from 'react-native-loading-spinner-overlay';
import color from "./src/styles/color";
import { Root } from 'native-base';

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isReady: false,
            isVisibleLoader: false
        };
    }

    async componentDidMount() {
        await Font.loadAsync({
            Roboto: require('native-base/Fonts/Roboto.ttf'),
            Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
            ...Ionicons.font,
        });

        this.setState({isReady: true});
    }

    isChangeVisibleLoader = (isVisibleLoader) => {
        this.setState({
            isVisibleLoader
        })
    }

    render() {
        if (!this.state.isReady) {
            return (
                <View
                    style={{
                        flex: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: color.primary
                    }}
                >
                    <ActivityIndicator
                        animating
                        size="large"
                        color="#FFFFFF"
                    />
                </View>
            );
        }

        return (
            <Provider store={store}>
                <Root>
                    <PersistGate
                        loading={
                            <View
                                style={{
                                    flex: 1,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    backgroundColor: '#087527'
                                }}
                            >
                                <ActivityIndicator
                                    animating
                                    size="large"
                                    color="#FFFFFF"
                                />
                            </View>
                        }
                        persistor={persistor}
                    >
                        <StatusBar
                            barStyle="light-content"
                            backgroundColor={'#2969c7'}
                            translucent={false}
                        />
                        <AppView/>
                        <DropdownAlert
                            ref={ref => DropDownHolder.setDropDown(ref)}
                            closeInterval={3000}
                            translucent
                            updateStatusBar={false}
                        />
                        <Spinner
                            ref={ref => LoaderApp.setLoaderApp(ref)}
                            visible={this.state.isVisibleLoader}
                            color={'white'}
                            overlayColor={color.primaryOverlay}
                            textStyle={{color: 'white'}}

                            isChangeVisibleLoader={(visible) => this.isChangeVisibleLoader(visible)}
                        />
                    </PersistGate>
                </Root>
            </Provider>
        );
    }
}

const styles = StyleSheet.create({
    preLoaderScreen: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'black'
    }
})

export default App
