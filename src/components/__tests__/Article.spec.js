/* eslint-disable no-undef */
import React from 'react';
import { shallow } from 'enzyme';

import {
  Article,
} from '../index';

describe('Article Component', () => {
  it('renders as expected', () => {
  const wrapper = shallow(
    <Article />,
  );
  expect(wrapper).toMatchSnapshot();
});
