import React, {Component} from 'react'
import {
    View,
    StyleSheet,
    Dimensions, TouchableWithoutFeedback, ScrollView, Text
} from 'react-native';
import RBSheet from "react-native-raw-bottom-sheet";
import color from "../styles/color";

const heightScreen = Dimensions.get('window').height;

class ModalHideBottom extends Component {
    constructor(props) {
        super(props)

        this.state = {}

        this.ModalHideBottom = React.createRef()
    }

    componentDidUpdate = (prevProps, prevState, snapshot) => {
        if (prevProps.open != this.props.open && this.props.open) {
            this.ModalHideBottom.open()
        }
        if (prevProps.open != this.props.open && !this.props.open) {
            this.ModalHideBottom.close()
        }
    }

    render() {


        return (
            <RBSheet
                ref={ref => {
                    this.ModalHideBottom = ref
                }}
                closeOnDragDown
                height={heightScreen - 50}
                onClose={() => this.props.close()}
                customStyles={{
                    wrapper: {
                        backgroundColor: color.primaryOverlay
                    }
                }}
            >
                <ScrollView showsVerticalScrollIndicator={false}>
                    <TouchableWithoutFeedback>
                        <View style={styles.gridContainer}>
                            { this.props.children }
                        </View>
                    </TouchableWithoutFeedback>
                </ScrollView>
            </RBSheet>
        )
    }
}

const styles = StyleSheet.create({
    gridContainer: {
        flex: 1
    }
})

export default ModalHideBottom
