/* eslint-disable no-undef */
import React from 'react';
import { shallow } from 'enzyme';

import {
  ArticleModal,
} from '../index';

describe('ArticleModal Component', () => {
  it('renders as expected', () => {
  const wrapper = shallow(
    <ArticleModal />,
  );
  expect(wrapper).toMatchSnapshot();
});
